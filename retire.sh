#!/bin/bash
docker stop server01
docker stop server02
docker rmi ansible-control
docker rmi ansible-centos
docker rm server01
docker rm server02
docker rm control
docker network rm ansible
rm -rf .ssh
rm key key.pub
rm -rf .ansible
rm inventory.ini
