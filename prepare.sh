#!/bin/bash
yes |ssh-keygen -f key
chmod 600 key

docker login
DOCKER_BUILDKIT=1
docker build --platform=linux/amd64 -t ansible-centos .
cd control
docker build --platform=linux/amd64 -t ansible-control .
cd ..
docker network create ansible

docker run --platform=linux/amd64 -dt --rm --network=ansible --name server01 ansible-centos
docker run --platform=linux/amd64 -dt --rm --network=ansible --name server02 ansible-centos

cat << EOF >> inventory.ini
[servers]
server01 ansible_host=`docker inspect -f "{{.NetworkSettings.Networks.ansible.IPAddress}}" server01`
server02 ansible_host=`docker inspect -f "{{.NetworkSettings.Networks.ansible.IPAddress}}" server02`

EOF
docker run --platform=linux/amd64 -it --rm --network=ansible -v $PWD:/root:z --name control ansible-control
